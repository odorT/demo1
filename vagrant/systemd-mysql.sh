#!/bin/bash

# Following part will create systemd service for our spring-petclinic. Note that environment 
# variables are passed there. The service will run java -jar command.

echo _______________________________________
echo Running systemd-mysql.sh script!!!
echo _______________________________________


cat > /etc/systemd/system/petclinic.service <<EOF

[Unit]
Description=Petclinic Application service powered with Spring Boot(with mysql database)

[Service]
User=app-user

# defining env vars
Environment="MYSQL_HOST=192.168.33.10"
Environment="MYSQL_PORT=3306"
Environment="MYSQL_NAME=demo1"
Environment="MYSQL_PASS=QWERT1234@"
Environment="MYSQL_USER=demo1"
Environment="MYSQL_URL=jdbc:mysql://db:3306/demo1"

WorkingDirectory=/home/app-user/demo1
ExecStart=/bin/java -Xms128m -Xmx256m -Dspring.profiles.active=mysql -jar /home/app-user/spring-petclinic-2.3.1.BUILD-SNAPSHOT.jar
SuccessExitStatus=143
TimeoutStopSec=10
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target

EOF

sudo systemctl daemon-reload
sudo systemctl enable petclinic.service
sudo systemctl stop petclinic
sudo systemctl start petclinic
