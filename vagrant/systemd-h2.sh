#!/bin/bash

# Following part will create systemd service for our spring-petclinic. Note that environment 
# variables are passed there. The service will run java -jar command.

echo _______________________________________
echo Running systemd-h2.sh script!!!
echo _______________________________________


cat > /etc/systemd/system/petclinic.service <<EOF

[Unit]
Description=Petclinic Application service powered with Spring Boot(with h2 database)

[Service]
User=app-user

WorkingDirectory=/home/app-user/demo1
ExecStart=/bin/java -Xms128m -Xmx256m -jar /home/app-user/spring-petclinic-2.3.1.BUILD-SNAPSHOT.jar
SuccessExitStatus=143
TimeoutStopSec=10
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target

EOF

sudo systemctl daemon-reload
sudo systemctl enable petclinic.service
sudo systemctl stop petclinic
sudo systemctl start petclinic
