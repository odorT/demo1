#!/bin/bash

# following loop will run in background and check if mysql database is accesible each 10 seconds.
# If mysql is accessible, it will run systemd-mysql.sh script which in terms serves the whole application.
# Note that if mysql is accessible, it will not restart the service until connection is lost.
# Once the conection is lost, script will call systemd-h2.sh which will also serve the app but with internal 
# h2 database. After losing connection, if mysql is accessible again, the progaram will switch to mysql.
### Note 
## Actually, this logic is not enaugh, because h2 is temporary database, and any changes will not be saved 
## permanently. I don't know if there is a way to implement it, but one thing I guess that if there is, it
## is totally complicated and not so effective. Best way to implement it is writing fallback logic in 
## application source code side - in java side. Therefore, I tried my best to keep the web application up;)

previous=none

while true; do
    sleep 10;
    mysql -u demo1 -h 192.168.33.10 -pQWERT1234@ -e "show databases"
    status=$?
    if [[ $status == 0 ]]; then
        if [[ $previous != "mysql" ]]; then
            echo "Using MySQL database" && sudo /bin/bash /vagrant/systemd-mysql.sh
            previous="mysql"
        elif [[ $previous == "mysql" ]]; then
            echo "Already using MySQL" 
            continue
        fi
    fi

    if [[ $status != 0 ]]; then
        if [[ $previous != "h2" ]]; then
            echo "Using h2 database" && sudo /bin/bash /vagrant/systemd-h2.sh
            previous="h2"
        elif [[ $previous == "h2" ]]; then
            echo "Already using H2"
            continue
        fi
    fi
done