#!/usr/bin/bash

echo _______________________________________
echo Running java.sh script!!!
echo _______________________________________

sudo apt-get update --fix-missing -y && sudo apt-get install openjdk-14-jdk mysql-client-core-8.0 git -y

# This function clones the remote repository and changes directory to it.
# If the directory exists, it pulls all updates
function git_resolve {
    dir=$(basename "$1" .git)
    echo "$dir"
    if [[ -d "$dir" ]]; then
      cd "$dir"
      git pull
    else
      git clone "$1" && cd "$dir"
    fi
}

git_resolve https://gitlab.com/odorT/demo1.git

sudo chmod +x mvnw

# Note that I build the package skipping checkstyle. This is because I faced problems when executing it in linux machine
# This problem appears because of CRLF and LF (windows && unix style line ending). If you wish, you can try it without 
# skipping it.
./mvnw clean package -Dcheckstyle.skip

cp /home/app-user/demo1/target/spring-petclinic-2.3.1.BUILD-SNAPSHOT.jar /home/app-user/spring-petclinic-2.3.1.BUILD-SNAPSHOT.jar
