#!/usr/bin/env bash

echo _______________________________________
echo Running db.sh script!!!
echo _______________________________________

DBNAME=demo1
DBUSER=demo1
DBPASSWD=QWERT1234@
ROOTPASSWD=QWERT1234@

sudo apt-get update --fix-missing -y && sudo apt-get install mysql-server -y


Q1="CREATE DATABASE IF NOT EXISTS $DBNAME;"
Q2="CREATE USER IF NOT EXISTS '$DBUSER'@'192.168.33.%' IDENTIFIED BY '$DBPASSWD';"
Q3="GRANT ALL ON $DBNAME.* TO '$DBUSER'@'192.168.33.%';"
Q4="FLUSH PRIVILEGES;"
SQL="${Q1}${Q2}${Q3}${Q4}"

# Running SQL queries
sudo mysql -uroot -p$ROOTPASSWD -e "$SQL"

# Changing the default bind address of mysql. Without it we will get connection errors. 
sudo sed -i "s/bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/mysql.conf.d/mysqld.cnf

sudo service mysql restart
