#!/bin/bash

echo _______________________________________
echo Running health-check.sh script!!!
echo _______________________________________

up=" [INFO] spring-petclinic: Web Application is up and running at localhost:8001 and 192.168.33.11:8080"
down=" [INFO] spring-petclinic: Web Application is down"

sleep 60;

# status1 will fetch json data from actuater health endpoint and if everything is fine, 
# it will become "UP", else error mesage. Note that this command called twice for re-checking.
# Because I faced some cases, where the application starts at first attempt, and sometimes at the second.
# I did not find the problem, but solution, restarting the service works pretty fine if there is not any
# other problem related to macen build, mysql connection or etc. Just remember that if you face
# "${down}" message after starting vagrant machines, and "${up}" after restarting the service, be sure
# that everything works as expected;)
status1=$(curl -s http://192.168.33.11:8080/actuator/health) | cut -d ':' -f2 | cut -d '"' -f2

if [[ status1 -eq "UP" && $(curl -s -o /dev/null -w "%{http_code}" 192.168.33.11:8080) -eq 200 ]]; then
	echo $up
else
	echo $down
	echo "Restarting the service, waiting 60 seconds" && sudo systemctl restart petclinic.service
	sleep 60;
	status2=$(curl -s http://192.168.33.11:8080/actuator/health) | cut -d ':' -f2 | cut -d '"' -f2
	if [[ status2 -eq "UP" && $(curl -s -o /dev/null -w "%{http_code}" 192.168.33.11:8080) -eq 200 ]]; then
		echo $up
	else
		echo $down
	fi
fi
