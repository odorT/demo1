# Deployment code for [spring-petclinic](https://github.com/spring-projects/spring-petclinic) project with [vagrant](https://www.vagrantup.com/).

## Usage:
1. Install vagrant and virtualbox and dos2unix(optional).
2. Clone this repository locally and change directory to demo1/vagrant folder.
3. (Optional) In case you use *nix OS run dos2unix *.
4. Run `vagrant validate` to check if there is any problem in `Vagrantfile`
5. If not, run `vagrant up` to install and provision machines.
6. After all, you can see petclinic website at `localhost:8001` or at `192.168.33.11:8080`.

## Notes:

### Vagrantfile:
There are 2 machines for the project(DB_VM and APP_VM). DB_VM hosts mysql server and is accessible from `192.168.33.10:3306` and `localhost:8002`.
APP_VM runs the project in via _app-user_ as a systemd service.

### java.sh:
This provision script will clone [this repository](https://gitlab.com/odorT/demo1.git) and start maven clean build.
**Note that there is -Dcheckstyle.skip option which ignores checkstyle. I encountered a problem while running the project in windows and linux. You can safely remove this part and check if works. In my case, checktyle interrupted the building procedure.**

### db.sh
It will create database called `demo1` and user called `demo1`. password for database is : QWERT1234@

### health-check.sh
After provisioning, this script will be run and check if the application is up and running. You will see the following message if everything proceeds successfully:
```APP_VM: _______________________________________
    APP_VM: Running health-check script!!!
    APP_VM: _______________________________________
    APP_VM:  [INFO] spring-petclinic: Server is up and running
```
If it fails, the petclinic service will be restarted.

### mysql-check.sh
This script will simply try to connect and run a query from mysql server in DB_VM, and if fails, will break the provisioning.